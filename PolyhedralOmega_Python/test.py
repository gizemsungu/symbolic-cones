# Import Polyhedral Omega
from lds import *
from util import *
from geometry import *
import json
import numpy as np
# Import time for timing
from time import *
import cProfile, pstats


def runPO(A, b, E, decomposition=True, parallelepipeds=True, rationalfunction=True, output=False):
    t1 = time()
    lds = LinearDiophantineSystem(A, b, E)

    if decomposition == True:
        cones = lds.symbolic_cones()

    if parallelepipeds == True:
        pis = lds.fundamental_parallelepipeds()

    if rationalfunction == True:
        r = lds.rational_function_string()
    return cones, pis,r


my_file=open("lhp5.txt","r")
readDimension = False
A = []
b = []
for line in my_file:
    if readDimension == False:
        dimensions = [int(x) for x in line.split(" ")]
        readDimension = True
    elif line.startswith("l") or line.startswith("n"):
        continue
    else:
        rows = [int(x) for x in line.split(" ")]

        A.append([-1*x for x in rows[1:]])
        b.append(rows[0])
my_file.close()
general_time1 = time()
cones, pis, r= runPO(A,b, E=None)
general_time2 = time()

print "coneS: ", cones, "\n"
print "pis: ", pis, "\n"
print "pis: ", r, "\n"
print "PO computation took: ", general_time2-general_time1, "secs"

#cProfile.runctx('runPO(A,b, E=None)', globals(), locals(), "test" + "Profile.prof")

#s = pstats.Stats("test" + "Profile.prof")
#s.strip_dirs().sort_stats("time").#print_stats()


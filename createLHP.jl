function LHP(dimension::Integer)
    A = Array{Int64, 2}(undef, dimension, dimension+1)

    for i=1:dimension
        if i == 1
            A[i,1] = 1
        else
            A[i,1] = 0
        end
        for j=1:dimension
            if i == 1 && i == j
                A[i, j+1] = -1*1
            elseif i != 1 && i == j
                A[i, j+1] = -1* (i - 1)
            elseif i == j + 1
                A[i, j+1] = -i* -1
            else
                A[i, j+1] = 0
            end
        end
    end

    #println(A)
    println(dimension , " ", dimension+1)
    for i in 1:dimension
        for j in 1:dimension+1
            print(A[i,j], " ")
        end
        println()
    end


end



LHP(10)

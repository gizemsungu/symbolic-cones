module PolyOmega
using LinearAlgebra, IterTools, MultivariatePolynomials, TypedPolynomials, AbstractAlgebra, TaylorSeries, BenchmarkTools, Dates
include("./SmithNormalForm/src/SmithNormalForm.jl")

function macmahon(A::Matrix{Int64}, b::Vector{Int64})
    sizeA = size(A)
    Id = Matrix(I, sizeA[2], sizeA[2])
    V = vcat(Id, A)
    q = append!(zeros(Float64, sizeA[2]), -b)
    o = zeros(Bool, sizeA[2])
    return (V, q, o)
end



function openness(j::Integer, o::Array{Bool,1}, equality::Integer)
    o_r = copy(o)
    if equality == 0
        o_r[j] = 0
        return o_r
    else
        deleteat!(o_r, j)
        return o_r
    end
end

function w(j::Integer, V::Array{Int64,2}, q::Array{Float64,1}, n::Integer )
    # q - q[n]/(V[j][n]) * V[j]
    return q[1:n-1] - ((q[n]/V[n,j]) * V[1:n-1, j])
end

function prim(V::Array{Int64,1})
    gcd_value = gcd(V)
    if gcd_value != 1
        return V/gcd_value
    end
    return V

end

function G(j::Integer, V::Array{Int64,2}, equality::Integer, sgqn::Integer, q::Array{Float64,1}, k::Integer, n::Integer)
    G_list = Array{Array{Int64,1},1}(undef, k)
    for i in 1:k
        if i == j && equality == 0
            G_r =  -1 * sgqn * V[1:n-1, j]
            G_list[i] = prim(G_r)
        else
            G_r = sgqn * (V[n, i] * V[1:n-1,j] - V[n, j] * V[1:n-1,i])
            G_list[i] = prim(G_r)
        end
    end
    return G_list
end

function addCone(SymbolicConeList::Dict{Any, Any}, cone::Tuple{Array{Int64,2},Array{Float64,1},Array{Bool,1}}, value::Integer)
    if haskey(SymbolicConeList, cone)
        SymbolicConeList[cone] = SymbolicConeList[cone] + value
        if SymbolicConeList[cone] == 0
            delete!(SymbolicConeList, cone)
        end
    else
        SymbolicConeList[cone] = value
    end
    return SymbolicConeList
end




function generateSubCones(J::Array{Int64,1}, k::Integer, n::Integer, V::Array{Int64,2}, equality::Integer, sgqn::Integer, q::Array{Float64,1}, o::Array{Bool,1}, sign::Integer)
    SymbolicConeList = Dict()
    for j in J
        G_r = G(j, V, equality, sgqn, q, k, n)
        w_r = w(j, V, q, n)
        o_r = openness(j, o, equality)
        flippedCone, flippedValue = flip((hcat(G_r...), w_r, o_r), sign)
        SymbolicConeList = addCone(SymbolicConeList, flippedCone, flippedValue)
    end
    return SymbolicConeList
end

function Cprime( V::Array{Int64,2}, q::Array{Float64,1}, o::Array{Bool,1}, n::Integer, k::Integer)
    _o = [o[i] for i in 1:k]
    _V = copy(V[1:n-1, :])
    _q = copy(q[1:n-1])
    return (_V, _q, _o)
end

function getGenerators(V::Array{Int64,2})
    generators = []
    k = size(V)[2]
    n = size(V)[1]
    for j in 1:k
        push!(generators, V[:,j])
    end
    return generators

end



function eliminate_last_coordinate(symbolicCones::Dict{Any, Any}, E::Vector{Int8})
    equalities = reverse(E)
    for equality in equalities
        newSymbolicCones = Dict()
        for (cone, value) in symbolicCones
            symbolicConeList = computeSymbolicCones(cone, equality, value)
            for (newCone, newValue) in symbolicConeList
                newSymbolicCones = addCone(newSymbolicCones, newCone, newValue)
            end
        end
        symbolicCones = newSymbolicCones

    end
    return symbolicCones
end

function vector_is_forward(j::Integer, n::Integer, V::Array{Int64,2})
    for i in 1:n
        if V[i, j] != 0
            return V[i,j] > 0
        end
    end
    return true
end

function flip(cone::Tuple{Array{Int64,2},Array{Float64,1},Array{Bool,1}}, sign::Integer)
    s = 1
    V = copy(cone[1])
    q = copy(cone[2])
    o = copy(cone[3])
    k = size(V)[2]
    n = size(V)[1]
    for j in 1:k
        isForward = vector_is_forward(j, n, cone[1])
        if !isForward
            V[:,j] = -1 * cone[1][:,j]
            s *= -1
        end
        if o[j] == isForward
            o[j] = true
        else
            o[j] = false
        end
    end
    sign = s * sign
    return (V, q, o), sign
end


function computeSymbolicCones(cone::Tuple{Array{Int64,2},Array{Float64,1},Array{Bool,1}}, equality::Integer, sign::Int64)

    V = copy(cone[1])
    q = copy(cone[2])
    o = copy(cone[3])
    k = size(V)[2]
    n = size(V)[1]
    sgqn = 1
    if q[n] < 0
        sgqn = -1
    end
    generatorIndices = V[n, 1:k] * sgqn
    J = Int64[]
    for j in 1:length(generatorIndices)
        if generatorIndices[j] < 0
            push!(J, j)
        elseif equality == 1 && generatorIndices[j] > 0 && q[n] == 0
            push!(J, j)
        end
    end
    if length(J) == 0
        SymbolicCones = Dict()
        flipCprime, flipCprimeValue = flip(Cprime(V, q, o, n, k), sign)
        return addCone(SymbolicCones, flipCprime, flipCprimeValue)
    elseif length(J) != 0 && sgqn == 1
        flipCprime, flipCprimeValue = flip(Cprime(V, q, o, n, k), sign)
        return addCone(generateSubCones(J, k, n, V, equality, sgqn, q, o, sign), flipCprime, flipCprimeValue)
    else
        return generateSubCones(J, k, n, V, equality, sgqn, q, o, sign)
    end
end


function enumerateFundamentalParallelePiped2(C::Tuple{Array{Int64,2},Array{Float64,1},Array{Bool,1}})

    V = sort(C[1], dims = 2)

    q = C[2]
    o = C[3]
    SMFRes = SmithNormalForm.smith(V)
    S = SmithNormalForm.diagm(SMFRes)
    Uinv = inv(SMFRes.S)
    Winv = inv(SMFRes.T)
    dimension = size(V, 2) # num of rows
    ambientDimension = size(V, 1) # num of cols

    diagonals = Int64[]
    for i in 1:dimension
        if(i <= size(S,1) && i <= size(S,2))
            push!(diagonals, S[i,i])
        end
    end


    lastDiagonal = diagonals[end]

    primeDiagonals = Int64[]
    for d in diagonals
        push!(primeDiagonals, Int64(lastDiagonal/d))
    end

    apex = q
    qhat = Uinv * apex
    Wprime = [Winv[j,i]*primeDiagonals[i] for i = 1:dimension, j = 1:dimension] #Winv * primeDiagonals
    tmpWprime = deepcopy(Wprime)

    for i in 1:dimension
        tmpWprime[dimension-i+1,:] = Wprime[i,:]
    end

    qtrans = [sum([-Wprime[j,i] * qhat[i] for i = 1:dimension]) for j = 1:dimension]
    qfrac = [qtrans[i] - floor.(Int, qtrans[i]) for i = 1:dimension]
    qint = [ floor.(Int, qi) for qi in qtrans ]
    qsummand = [round(Int, qi) for qi in (lastDiagonal*apex + V*qfrac) ]
    openness = [ (qfrac[j] == 0 ? o[j] : 0) for j in 1:ambientDimension]
    ##println("Wprime: ", Wprime, " qint: ", qint, " qsummand: ", qsummand, " openness: ", openness, " S[k,k] ", S[ambientDimension,ambientDimension], " qhat ", qhat)

    L = []
    P = []
    for v in IterTools.product([0:diagonals[i]-1 for i in 1:dimension]...)
        push!(P, v)
        innerRes = []
        j = 1
        for qj in qint
            inner = 0
            i = 1
            for vi in v
                inner += Wprime[i,j] * vi
                i += 1
            end
            inner += qj
            inner = inner % lastDiagonal

            if inner == 0 && o[j]
                inner = lastDiagonal
            end
            append!(innerRes, inner)
            j += 1
        end

        outerRes = []
        for l in 1:ambientDimension
            outer = 0
            j = 1
            for innerResi in innerRes
                outer += V[l,j] * innerResi
                j += 1
            end
            append!(outerRes, outer) # outerRes is an integral vector
        end
        push!(L, collect(round(Int, (ai + bi) / lastDiagonal) for (ai,bi) in collect(zip(outerRes, qsummand)) ))
    end
    return L
end


function enumerateFundamentalParallelePiped(cone::Tuple{Array{Int64,2},Array{Float64,1},Array{Bool,1}})
    #println("####################MINE#################")
    V = cone[1]
    q = cone[2]
    o = cone[3]
    #println(det(V))
    k = size(V)[2]
    n = size(V)[1]
    #println("V: ", getGenerators(V))
    SMFRes = SmithNormalForm.smith(V)
    S = SmithNormalForm.diagm(SMFRes)
    Uinv = inv(SMFRes.S)
    Winv = inv(SMFRes.T)
    #println("S: ", S, " Uinv ", SMFRes.S, " Winv ", SMFRes.T)
    sPrime = Int64[]
    for i in 1:k
        if i <= k && i <= n
            push!(sPrime, floor(Int64, (S[k,k] / S[i,i])))
        end
    end
    #println("Sprime : ", sPrime)

    qhat = Uinv * q
    Wprime = [ Winv[:,j] * sPrime[j] for j in 1:k]
    qfrac = Float64[]

    qint = Int64[]
    for j in 1:k
        sumq = 0
        for i in 1:k
            sumq += -Wprime[j][i] * qhat[i]
        end
        push!(qint, floor(sumq))
        push!(qfrac, sumq - floor(sumq))
    end
    qsummand = [qi for qi in (S[k,k]*q + V*qfrac)]
    openness = [ (qfrac[j] == 0 ? o[j] : false) for j in 1:k]



    cpl = []
    for j in 1:k
        push!(cpl,[i for i in 1:length(Diagonal(S)[j])])
        #push!(cpl,[1])
    end
    P = Base.product(cpl...)

    return [transform_integral([v...], Wprime, qint, n, qsummand, S[k,k], openness, V) for v in P]

end


function transform_integral(v::Array{Int64,1}, Wprime::Array{Array{Float64,1},1}, qint::Array{Int64,1}, n::Int64, qsummand::Array{Float64,1}, sk::Int64, openness::Array{Bool,1}, V::Array{Int64,2})
    innerRes = []
    j = 1
    for qj in qint
        inner = 0
        i = 1
        for vi in v
            inner += vi * Wprime[j][i]
            i += 1
        end
        inner += qj

        inner %= sk

        if inner == 0 && openness[j]
            inner = sk
        end
        push!(innerRes, inner)
        j+=1
    end

    outerRes = []
    for i in 1:n
        outer = 0
        j = 1
        for innerResi in innerRes
            outer += V[i,j] * innerResi
            j += 1
        end
        push!(outerRes, outer)

    end

    return [ floor((ai + bi)/sk) for (ai,bi) in zip(outerRes, qsummand)]

end

function computeRationalFunction(C::Tuple{Array{Int64,2},Array{Float64,1},Array{Bool,1},Array{Any,1},Int64})
    V = C[1]
    q = C[2]
    o = C[3]
    fp = C[4]
    sign = C[5]
    numOfVars = size(q,1)
    VV = [string("x_",i) for i in 1:numOfVars]
    S, X = PolynomialRing(QQ, VV, ordering=:deglex)
    num = 0
    ##println("VV: ", VV, "\nQQ: ", QQ, "\nS: ", S, "\nX: ", X, "\n")
    for p in fp
        tmp = 1
        for i in 1:length(p)
            if p[i] < 0
                tmp *=X[i]^-p[i]
            else
                tmp *=X[i]^p[i]
            end
            tmp *= sign
        end
        num += tmp
    end
    ##println("Numerator:", num)
    den = 1
    for j in 1:size(V,2)
        tmp = 1
        for i in 1:size(V,1)
            if V[:,j][i] < 0
                tmp *=X[i]^- V[:,j][i]
            else
                tmp *=X[i]^ V[:,j][i]
            end
        end
        den *= 1 - tmp
    end
    ##println("Denominator:", den)
    ratfun = num//den
    ##println("ratfun:", ratfun)

    return ratfun
end

function readFile(FileName::String)
   bA = []
   E = []
   rowCol = []
   open(FileName) do f
      line = 0


      # read till end of file
      while ! eof(f)
         s = readline(f)
         if line == 0
            rowCol = [parse(Int, ss) for ss in split(s)]
            E = vec(zeros(Int8, 1, rowCol[1]))
         elseif startswith(s, "linearity") == false && startswith(s, "nonnegative") == false
            int_s = [parse(Int, ss) for ss in split(s)]
            push!(bA, int_s)

         """else
            if startswith(s, "linearity")
               s = replace.(s, "linearity " => "")
               linearity = [parse(Int, ss) for ss in split(s)]
               E = vec(zeros(Int8, 1, linearity[1]))
               for i in 2:length(linearity)
                  E[linearity[i]] = 1
               end
           end"""
         end


         line += 1
      end

   end
   return rowCol, bA, E
end


function createLDS(LDS::Tuple{Array{Int64, 1}, Array{Any, 1}, Array{Int8,1}})
   rowCol = LDS[1]
   bA = hcat(LDS[2]...)
   bA = transpose(bA)
   b = bA[:,1]
   A = -1 * bA[:, 2:rowCol[2]]
   return (A, b, LDS[3])
end



function test()
    A, b, E = createLDS(readFile("../symbolic-cones/lhp5.txt"))
    E = vec(zeros(Int8, 1, size(A)[1]))
    b = vec(b)
    """A = [1 0 0;-2 1 0; 0 -3 2]
    #println("row: ", size(A)[1], "column: ", size(A)[2])
    b = vec([1 0 0])"""
    E = vec(zeros(Int8, 1, size(A)[1]))
    symbolicCones = Dict()
    symbolicCones[macmahon(A, b)] = 1
    symbolicCones = eliminate_last_coordinate(symbolicCones, E)
    enumeratedCones = []
    for (cone,sign) in symbolicCones
        push!(enumeratedCones, (cone[1], cone[2], cone[3], enumerateFundamentalParallelePiped2(cone), sign))
    end

    """enumeratedCones = []
    for (cone,sign) in symbolicCones
        push!(enumeratedCones, (cone[1], cone[2], cone[3], enumerateFundamentalParallelePiped(cone), sign))
    end"""

    for eCone in enumeratedCones
        ##println(eCone)
        println(computeRationalFunction(eCone))
    end



end
time1 = now()
#@belapsed test()
"""time2 = now()
elap = time2-time1
println(elap)"""

bl = @benchmark test() seconds=1 time_tolerance=0.01
println(bl)




end
